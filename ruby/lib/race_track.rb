
class RaceTrack
  attr_reader :name


  def initialize(data)
    @name = data['race']['track']['name']
    puts "|~| Analyze racetrack #{name}"
    
    @pieces = Array.new
    data['race']['track']['pieces'].each do |piece|
      new_piece = TrackPiece.new(piece)
      @pieces << new_piece
    end
    puts "|~| #{@pieces.length} track pieces found"
  end

  def get_piece(index)
    @pieces[index]
  end

  def next_piece(index)
    if (index + 1) < @pieces.length
     @pieces[index + 1]
    else
      @pieces[index - @pieces.length]
    end
  end
end