class TrackPiece
  attr_reader :length
  attr_reader :speed

  def initialize(data)
    @length = data["length"]
    @speed = 1
    if data["switch"] 
      @switch = true
    end
  end

  def is_curve
    !is_straight
  end

  def is_straight
    !@length.nil?
  end

  def is_switch
    !@switch.nil?
  end

  def to_s
   "TrackPiece #{@speed} [curved = #{is_curve},length = #{@length},switch = #{is_switch}]"
  end
end