class Brain


  def initialize(racetrack)
    @racetrack = racetrack
    @current_piece = -1
  end

  def predict_the_future() 
    puts "|~| Brain is starting predicting the future...bzzzzzzz"
  end

  def throttle(piece_position, current_speed)
    factor = 1
    if @racetrack.next_piece(@current_piece).is_curve && @racetrack.next_piece(@current_piece+1).is_curve 
      if current_speed < 14
        factor = 0.2
      end
    end

     if @racetrack.get_piece(@current_piece).is_curve && @racetrack.next_piece(@current_piece).is_straight 
      factor = 1
    end


    if  @current_piece != piece_position 
       @current_piece  = piece_position
       puts "|~| #{@current_piece} | curve" if @racetrack.get_piece(@current_piece).is_curve
       puts "|~| #{@current_piece} | straight" if @racetrack.get_piece(@current_piece).is_straight
     
       puts "|~| #{@current_piece} | curve" if @racetrack.next_piece(@current_piece).is_curve
       puts "|~| #{@current_piece} | straight" if @racetrack.next_piece(@current_piece).is_straight
     
       puts "|~| #{@current_piece} | curve" if @racetrack.next_piece(@current_piece+1).is_curve
       puts "|~| #{@current_piece} | straight" if @racetrack.next_piece(@current_piece+1).is_straight
     

       puts "|~| throttle :: #{@racetrack.get_piece(@current_piece).speed * factor}".colorize(color: :red)
    end
  
    @racetrack.get_piece(@current_piece).speed * factor
  end

   def switch?(piece_position, current_speed)
    if  @racetrack.next_piece(@current_piece).is_switch && @racetrack.next_piece(@current_piece+1).is_curve && @racetrack.next_piece(@current_piece+2).is_curve 
      return true
    end
    return false
  end

end