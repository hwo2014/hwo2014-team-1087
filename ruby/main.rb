require 'json'
require 'socket'
require 'colorize'
require './lib/race_track.rb'
require './lib/track_piece.rb'
require './lib/brain.rb'



server_host = ARGV[0]
server_port = ARGV[1]
bot_name = ARGV[2]
bot_key = ARGV[3]

VERSION = '0.0.1'
BOT_NAME = 'UberSmokerBot'

system "clear"
puts "I'm the #{BOT_NAME} V#{VERSION} // #{bot_name}".colorize(color: :blue, background: :white)
puts "|~| Connect to #{server_host}:#{server_port}"

class UberSmokerBot

  def initialize(server_host, server_port, bot_name, bot_key)
    tcp = TCPSocket.open(server_host, server_port)
    play(bot_name, bot_key, tcp)
    @bot_name = bot_name
  end

  def test_brain
    test_track_data =  make_msg("gameInit", race: {track: {id: 'zandvoort' , name: 'Zandvoort', 
      pieces: [
          {length: 100},
          {length: 100},
          {length: 100, switch: true},
          {radius: 200, angle: 22.5}
      ]


      } })
    @racetrack = RaceTrack.new(JSON.parse(test_track_data))
    @brain = Brain.new(@racetrack)
    @brain.predict_the_future()
    puts @brain.throttle(0,0)
  end

  private

  def play(bot_name, bot_key, tcp)
    tcp.puts join_message(bot_name, bot_key)
    react_to_messages_from_server tcp
  end

  def react_to_messages_from_server(tcp)
    while json = tcp.gets
      message = JSON.parse(json)
      msgType = message['msgType']
      msgData = message['data']
      if message['gameTick']
       gameTick = message['gameTick']
      end
      case msgType
        when 'carPositions'

         #run brain after 5 gameTicks
         if msgData[0]['piecePosition']["pieceIndex"] !=  @current_piece
            @current_piece =msgData[0]['piecePosition']["pieceIndex"]
            @current_speed = (gameTick - @current_time)
            system "clear"

            if @current_piece == 2 
               puts "switch"
               tcp.puts switch_message(:left)
            end

            puts "#{BOT_NAME} V#{VERSION} // running".colorize(color: :blue, background: :white)
            puts "|~| track piece time (speed in ticks) = #{@current_speed}"
            @current_time =  gameTick
         end
         tcp.puts throttle_message(@brain.throttle(@current_piece,@current_speed))
        
        else
          case msgType
            when 'join'
              puts '|~| Join race'
            when 'gameInit'
              @racetrack = RaceTrack.new(msgData)
              @brain = Brain.new(@racetrack)
              @brain.predict_the_future()
              @current_piece = 0
              @current_speed = 1

            when 'gameStart'
               puts '|~| Race started'
               @current_time = 0
            when 'crash'
              puts 'Someone crashed'
            when 'gameEnd'
              puts '|~| Race ended'
              best_time = msgData['bestLaps'][0]['result']['millis'] / 1000.0
              puts "|~| Best track time = #{best_time}"
            when 'error'
              puts "ERROR: #{msgData}"
          end
          #puts "Got #{msgType}"
          tcp.puts ping_message
      end
    end
  end

  def join_message(bot_name, bot_key)
    make_msg("join", {:name => bot_name, :key => bot_key})
  end

  def throttle_message(throttle)
    make_msg("throttle", throttle)
  end

   def switch_message(direction)
    if direction = :left
      make_msg("switchLane", "Left")
    else
      make_msg("switchLane", "Right")
    end
  end

  def ping_message
    make_msg("ping", {})
  end

  def make_msg(msgType, data)
    JSON.generate({:msgType => msgType, :data => data})
  end
end

UberSmokerBot.new(server_host, server_port, bot_name, bot_key)
